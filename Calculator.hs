-- Help functions
help = "This is a Haskell calculator. Functions are separated into topic groups. To access a list of those type in: (help-simple), (help-circle), (help_rectangular)";
help_simple = "Functions: (add_int) for the addition of integers, (add_double) for the addition of doubles, (sub_int) for the subtraction of integers, (sub_double) for the subtraction of doubles, (mul_int) for the multiplication of integers, (mul_double) for the multiplication of doubles, (div_int) for the division of integers, (div_double) for the division of doubles, (pow_int) for exponents with integers, (pow_double) for exponents with doubles, (per_int) for the percentage of an integer to another integer, (per_double) for the percentage of a double to another double"
help_circle = "Functions: (circumference) for the circumference of a circle, (radius) for the radius of a circle, (diameter) for the diameter of a circle, (circlearea) for the area of a circle, (sphere_volume) for the volume of a sphere, (sphere_surface_area) for the surface area of a sphere, (cone_volume) for the volume of a cone, (cone_surface_area) for the surface area of a cone, (cylinder_volume) for the volume of a cylinder, (cylinder_surface_area) for the surface area of a cylinder"
help_rectangular = "Functions: (perimeter_int) to calculate the perimeter of a rectangle with integers, (perimeter_double) to calculate the perimeter of a rectangle with doubles, (is_square_int) to calculate if a rectangle is also a square with sides measured as integers, (is_square_double) to calculate if a rectangle is also a square with sides measured as doubles, (rectangle_area_int) to calculate the area of a rectangle with integers, (rectangle_area_double) to calculate the area of a rectangle with doubles, (rectangular_prism_volume_int) to calculate the area of a rectangular prism with integers, (rectangular_prism_volume_double) to calculate the area of a rectangular prism with doubles, (is_cube_int) calculates if all three sides are the same length measured as integers, (is_cube_double) calculates if all three sides are the same length measured as doubles, (rectangular_prism_perimeter_int) calculates the perimeter of a rectangle with integers, (rectangular_prism_perimeter_double) calculates the perimeter of a rectangle with doubles)"

-- Set numbers
num_e = 2.71828182846
num_pi = 3.14159265359

-- Simple Calculator Functions
add_int :: Int -> Int -> Int
add_int x y = x + y

add_double :: Double -> Double -> Double
add_double x y = x + y

sub_int :: Int -> Int -> Int
sub_int x y = x - y

sub_double :: Double -> Double -> Double
sub_double x y = x - y

mul_int :: Int -> Int -> Int
mul_int x y = x * y

mul_double :: Double -> Double -> Double
mul_double x y = x * y

div_int :: Int -> Int -> Int
div_int x y = x `div` y

div_double :: Double -> Double -> Double
div_double x y = x / y

pow_int :: Int -> Int -> Int
pow_int x y = x ^ y

pow_double :: Double -> Double -> Double
pow_double x y = x ** y

per_int :: Int -> Int -> Int
per_int x y = x `div` y * 100

per_double :: Double -> Double -> Double
per_double x y = x / y * 100

-- Complex Calculator Functions
-- sqrt x
-- ans
-- sin x
-- cos x
-- tan x

-- Circle/Circular Calculator Functions
circumference :: Double -> Double
circumference dia = dia * num_pi

radius :: Double -> Double
radius dia = dia / 2

diameter :: Double -> Double
diameter rad = rad * 2

circlearea :: Double -> Double
circlearea rad = (pow_double rad 2) * num_pi

sphere_volume :: Double -> Double
sphere_volume rad = 4 / 3 * num_pi * (pow_double rad 3)

sphere_surface_area :: Double -> Double
sphere_surface_area rad = 4 * num_pi * (pow_double rad 2)

cone_volume :: Double -> Double -> Double
cone_volume rad height = num_pi * (pow_double rad 2) * height / 3

cone_surface_area :: Double -> Double -> Double
cone_surface_area rad height = num_pi * rad * (rad + sqrt((pow_double height 2) + (pow_double rad 2)))

cylinder_volume :: Double -> Double -> Double
cylinder_volume rad height = num_pi * (pow_double rad 2) * height

cylinder_surface_area :: Double -> Double -> Double
cylinder_surface_area rad height = 2 * num_pi * rad * height + 2 * num_pi * (pow_double rad 2)

-- Rectangular/Cubic Calculator Functions
perimeter_int :: Int -> Int -> Int
perimeter_int side1 side2 = 2 * side1 + 2 * side2

perimeter_double :: Double -> Double -> Double
perimeter_double side1 side2 = 2 * side1 + 2 * side2

is_square_int :: Int -> Int -> Bool
is_square_int side1 side2 = side1 == side2

is_square_double :: Double -> Double -> Bool
is_square_double side1 side2 = side1 == side2

rectangle_area_int :: Int -> Int -> Int
rectangle_area_int side1 side2 = side1 * side2

rectangle_area_double :: Double -> Double -> Double
rectangle_area_double side1 side2 = side1 * side2

rectangular_prism_volume_int :: Int -> Int -> Int -> Int
rectangular_prism_volume_int side1 side2 side3 = side1 * side2 * side3

rectangular_prism_volume_double :: Double -> Double ->  Double -> Double
rectangular_prism_volume_double side1 side2 side3 = side1 * side2 * side3

is_cube_int :: Int -> Int -> Int -> Bool
is_cube_int side1 side2 side3 = side1 == side2 && side1 == side3

is_cube_double :: Double -> Double -> Double -> Bool
is_cube_double side1 side2 side3 = side1 == side2 && side1 == side3

rectangular_prism_perimeter_int :: Int -> Int -> Int -> Int
rectangular_prism_perimeter_int side1 side2 side3 = 4 * (side1 + side2 + side3)

rectangular_prism_perimeter_double :: Double -> Double -> Double -> Double
rectangular_prism_perimeter_double side1 side2 side3 = 4 * (side1 + side2 + side3)