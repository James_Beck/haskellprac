-- Variables and functions
area :: Float -> Float
area n = pi * n^2

rectArea :: Float -> Float -> Float
rectArea width height = width * height

squareArea :: Float -> Float
squareArea height = rectArea height height

boxVolume :: Float -> Float -> Float -> Float
boxVolume height width breath = height * width * breath

heron :: Float -> Float -> Float -> Float
heron sideA sideB sideC = sqrt (s * (s - sideA) * (s - sideB) * (s - sideC))
                          where
                          s = (sideA + sideB + sideC) / 2

areaTriangleTrig :: Float -> Float -> Float -> Float
areaTriangleTrig sideA sideB sideC = sideC * height / 2
                                     where
                                        cosa = (sideB ^ 2 + sideC ^ 2 - sideA ^ 2) / (2 * sideB * sideC)
                                        sina = sqrt (1 - cosa ^ 2)
                                        height = sideB * sina

-- Summary
-- 1. Variables store values (which can be any arbitrary Haskell expression).
-- 2. Variables do not change within a scope.
-- 3. Functions help you write reusable code.
-- 4. Functions can accept more than one parameter.

-- Truth values
absolute :: Float -> Float
absolute x 
    | x < 0     = -x
    | otherwise = x

whatCanIDo :: Int -> [Char]
whatCanIDo age
    | age < 16  = "Well you can't do much"
    | age < 18  = "Oh, you can get married and have sex"
    | age < 19  = "You can drink"
    | age < 21  = "You can play in the local casino"
    | otherwise = "You are a fully fledged adult"

numOfRealSolutions ax xy yc
    | discriminate > 0 = 2
    | discriminate == 0 = 1
    | otherwise = 0
        where
        discriminate = xy ^ 2 - 4 * ax * yc

-- Type basics
-- The type of negate is Num a => a -> a
-- The type of (||) is Bool -> Bool -> Bool
-- The type of monthLength is Bool -> Int -> Int
-- The type of f x y is Bool -> Bool -> Bool
-- The type of g x is Num a => a -> a
xor :: Bool -> Bool -> Bool
xor bool1 bool2 = (bool1 || bool2) && not (bool1 && bool2)

-- Lists and Tuples
cons8 :: Num a => [a] -> [a]
cons8 (x:xs) = 8:(x:xs)

cons8End :: Num a => [a] -> [a]
cons8End (x:xs) = (x:xs) ++ [8]

myCons :: [a] -> a -> [a]
myCons list appendingThing = list ++ [appendingThing]

exampleTuple = (3,"hello",True)
extractTest = (("hello", 4), True)

fifthInList :: [a] -> [a]
fifthInList list
    | length list == 5 = (last list):[]
    | length list > 5  = fifthInList (init list)
    | otherwise        = []

-- Summary
-- 1. Lists are defined by square brackets and commas: [1,2,3]
-- 2. Tuples ar edefined by parentheses and commas: ("Bob",32)
-- 3. Lists and tuples can be combined in any number of ways: lists within lists, tuples within lists,
-- etc, but their criteria must still be fulfilled for combinations to be valid

-- Type basics II
-- Just read about Num and how it's polymorphic to be any type of number (float, int, integer, double)
-- fromIntegral is a great example of taking a type int and converting it to the polymorphic type numOfRealSolutions

-- Next Steps
numberRange :: (Ord a, Num a) => a -> a -- Ord seems to be something required when data is being compared
numberRange a
    | a < 0     = -1
    | a > 0     = 1
    | otherwise = a

points :: (Eq a, Num a) => a -> a 
-- This could've allowed for the result to be a different type of num
-- points :: (Eq a, Num a, Num b) => a -> b
points x = 
    if x == 1
        then 10
        else if x == 2
            then 6
            else if x == 3
                then 4
                else if x == 4
                    then 3
                    else if x == 5
                        then 2
                        else if x == 6
                            then 1
                            else 0
pointsEdited :: (Eq a, Num a, Num b) => a -> b
pointsEdited 1 = 10
pointsEdited 2 = 6
pointsEdited 3 = 4
pointsEdited 4 = 3
pointsEdited 5 = 2
pointsEdited 6 = 1
pointsEdited _ = 0

mixingPointStyles :: (Num a, Ord a) => a -> a
mixingPointStyles 1 = 10
mixingPointStyles 2 = 6
mixingPointStyles x
    | x <= 6    = 7 - x
    | otherwise = 0

-- Building vocabulary
-- Didn't go over anything new, just read some stuff about Haskell

-- Simple input and output
requestName :: IO ()
requestName = do
    putStrLn "Please enter your name:"
    name <- getLine
    putStrLn ("Hello " ++ name ++", how do you do?")


infoAboutATriangle = do
    putStrLn "What is the base of the triange?"
    base <- getLine
    putStrLn "Great. Now what is the height of the triangle?"
    height <- getLine
    let area = ((read base :: Float) * (read height :: Float)) / 2
    return area

areaOfTriangleIs = do
    result <- infoAboutATriangle
    putStrLn $ "The area of that triable is " ++ show result

guessTheNumber num = do
    putStrLn "Enter your guess:"
    guess <- getLine
    if (read guess) < num
        then do putStrLn "Too low!"
                guessTheNumber num
        else if (read guess) > num
                 then do putStrLn "Too high!"
                         guessTheNumber num
                 else putStrLn "You Win!"

iKnowYou = do
    putStrLn "What's your name?"
    name <- getLine
    if (read name) == "Simon" || (read name) == "John" || (read name) == "Phil"
        then putStrLn "Haskell is great"
        else if (read name) == "Koen"
                 then putStrLn "Debugging Haskell is great"
                 else putStrLn "I don't know who you are"

