-- Recursion
factorial :: Int -> Int
factorial n
    | n == 0 = 1
    | n > 0 = n * factorial (n - 1)
    | otherwise = 0

doubleFactorial :: Int -> Int
doubleFactorial n
    | n == 0 = 1
    | n == 1 = 1
    | n > 0 = n * doubleFactorial (n - 2)
    | otherwise = 0

mult :: (Eq a, Num a) => a -> a -> a
mult _ 0 = 0
mult n m = (mult n (m - 1)) + n

-- Expansion of mult 5 4 example
-- 4 isn't 0 so we apply mult n 3
---- 3 isn't 0 so we apply mult n 2
------ 2 isn't 0 so we apply mult n 1
-------- 1 isn't 0 so we apply mult n 0
---------- 0 is found so we start with a value of 0
-------- +5 is added from mult n 1
------ +5 is added from mult n 2
---- +5 is added from mult n 3
-- + 5 is added from mult n 4
-- our final result is that mult 5 4 gives you 20

powerOf :: Int -> Int -> Int
powerOf _ 0 = 1
powerOf x y = powerOf x (y - 1) * x

addition :: (Eq a, Num a) => a -> a -> a
addition a 0 = a
addition a b = addition a (b - 1) + 1

log2 :: Int -> Int
log2 1 = 0
log2 n = 1 + log2 (n `div` 2)

replication :: Int -> a -> [a]
replication 0 _ = []
replication 1 y = [y]
replication x y = replication (x - 1) y ++ [y]

exclaimExclaim :: [a] -> Int -> a
exclaimExclaim (x:xs) 0 = x
exclaimExclaim (x:xs) y = exclaimExclaim xs (y - 1)

zippity :: [a] -> [b] -> [(a,b)]
zippity [] [] = []
zippity (x:xs) [] = []
zippity [] (y:ys) = []
zippity (x:xs) (y:ys) = [(x,y)] ++ zippity xs ys

-- Lists II
doubleList :: [Int] -> [Int]
doubleList [] = []
doubleList (x:xs) = (2 * x) : doubleList xs

tripleList :: [Int] -> [Int]
tripleList (x:xs) = (3 * x) : tripleList xs

-- There's a dry principle that we are violating here, 'r' the not to reuse code
-- The only difference between these two functions is the doubleList has (2 * n) and tripleList has (3 * n)

multList :: Int -> [Int] -> [Int]
multList _ [] = []
multList x (y:ys) = (x * y) : multList x ys

takeInt :: Int -> [a] -> [a]
takeInt 0 _ = []
takeInt x (y:ys) = [y] ++ takeInt (x - 1) ys

dropInt :: Int -> [a] -> [a]
dropInt 0 (y:ys) = (y:ys)
dropInt _ [] = []
dropInt x (y:ys) = dropInt (x - 1) ys

sumInt :: [Int] -> Int
sumInt [] = 0
sumInt (x:xs) = x + sumInt xs

scanSum :: [Int] -> [Int]
scanSum [] = []
scanSum singleList@[x] = singleList
scanSum (x:y:xs) = [x] ++ scanSum ((x + y) : xs)

diffs :: [Int] -> [Int]
diffs [] = []
diffs [x] = []
diffs (x:y:xs) = [x - y] ++ diffs(y:xs)

applyToIntegers :: (Int -> Int) -> [Int] -> [Int]
applyToIntegers _ [] = []
applyToIntegers func (x:xs) = (func x) : applyToIntegers func xs

multiplyList :: Int -> [Int] -> [Int]
multiplyList m = applyToIntegers ((*) m)

timesNeg1 :: Int -> Int
timesNeg1 x = x * (-1)

invertList :: [Int] -> [Int]
invertList [] = []
invertList (x:xs) = map timesNeg1 (x:xs)

divisors x = [ y | y <- [1..x], x `mod` y == 0]

listOfDivisors :: [Int] -> [[Int]]
listOfDivisors [] = []
listOfDivisors (x:xs) = map divisors (x:xs)

inCombination :: [Int] -> [[Int]]
inCombination = map (invertList . divisors)

customTail :: [Int] -> Int
customTail [] = error "Hey, no list bro"
customTail [x] = x
customTail (x:xs) = customTail xs

customInit :: [Int] -> [Int]
customInit [] = error "Hey, no list bro"
customInit [x] = []
customInit (x:xs) = x : customInit xs

-- Type declarations
data Anniversary = Birthday String Date
                 | Wedding String String Date

data Date = Date Int Int Int

johnSmith :: Anniversary
johnSmith = Birthday "John Smith" (Date 3 7 1968)

smithWedding :: Anniversary
smithWedding = Wedding "John Smith" "Jane Smith" (Date 4 3 1987)

anniversariesOfJohnSmith :: [Anniversary]
anniversariesOfJohnSmith = [johnSmith, smithWedding]

showDate :: Date -> String
showDate (Date day month year) = show day ++ "-" ++ show month ++ "-" ++ show year

showAnniversary :: Anniversary -> String
showAnniversary (Birthday name date) = name ++ ", born " ++ showDate date
showAnniversary (Wedding name1 name2 date) = name1 ++ " married " ++ name2 ++ " on " ++ showDate date

-- Pattern matching
data Foo = Bar | Baz Int

fooTest :: Foo -> Int
fooTest Bar = 1
fooTest (Baz x) = x - 1

valueFoo :: Foo
valueFoo = Baz 10

showFoo :: Foo -> String
showFoo (Baz x) = show x

dropThree :: [a] -> [a] 
dropThree (_:_:_:xs) = xs
dropThree _ = []

fstPlusSnd :: (Num a) => (a, a) -> a
fstPlusSnd (x, y) = x + y

norm3D :: (Floating a) => (a, a, a) -> a
norm3D (x, y, z) = sqrt (x^2 + y^2 + z^2)

-- This one is basically if we see a 1 then the state has changed.
binaryTrueFalse :: [Int] -> Bool
binaryTrueFalse [] = True
binaryTrueFalse (1:xs) = not (binaryTrueFalse xs)
binaryTrueFalse (0:xs) = binaryTrueFalse xs
binaryTrueFalse (_:xs) = error "the input is not binary"

data Foo2 = Bar2 | Baz2 {bazNumber::Int, bazName::String}

testFoo2 :: Foo2 -> Int
testFoo2 Baz2 {bazName=name} = length name
testFoo2 Bar2 {} = 0

valueFoo2 :: Foo2
valueFoo2 = Baz2 1 "Haskell"

showFoo2 :: Foo2 -> String
showFoo2 (Baz2 x y) = show x ++ " " ++ y

letClauseEx = let (x:_) = map (*2) [1,2,3]
              in x + 5

whereClauseEx = x + 5
                where
                (x:_) = map (*2) [1,2,3]


catMaybes :: [Maybe a] -> [a]
catMaybes ms = [ x | Just x <- ms ]

putFirstChar = do
    (x:_) <- getLine
    putStrLn [x]

-- Control Structures
describeLetter :: Char -> [Char]
describeLetter c
    | c >= 'a' && c <= 'z' = "Lower case"
    | c >= 'A' && c <= 'z' = "Upper case"
    | otherwise            = "Not a letter, hmm..."

whatCanIDoAgain :: Int -> [Char]
whatCanIDoAgain x =
    case x of
        16 -> "You can get married"
        18 -> "You can drink"
        20 -> "You can go to the casino"
        _ -> "Eh, doesn't matter"

allTheChars :: Int -> [Char] -> [Char]
allTheChars _ [] = []
allTheChars 0 (x:xs) = "The final value is " ++ [x]
allTheChars n (x:xs) = "The char at position is " ++ [x] ++ allTheChars (n - 1) xs

data Colour = Black 
            | White 
            | RGB Int Int Int

describeColour :: Colour -> String
describeColour colour = 
    "This colour is "
    ++ case colour of
        Black -> "black"
        White -> "white"
        RGB 0 0 0 -> "black"
        RGB 255 255 255 -> "white"
        RGB 255 0 0 -> "red"
        RGB 0 255 0 -> "green"
        RGB 0 0 255 -> "blue"
        _           -> "uhh, some colour"
    ++ ", yeah?"

fakeIf :: Bool -> [Char] -> [Char] -> [Char]
fakeIf condition x y =
    case condition of
    True -> x
    False -> y

-- More on functions
tail' = (\ (_:xs) -> xs)

lambdaPrac1 = (\ x -> x * 2 + 3)
lambdaPrac2 = (\ x y -> read x + y)

-- Higher order functions
quickSort :: (Ord a) => [a] -> [a]
quickSort [] = []
quickSort (x:xs) = (quickSort less) ++ (x:equal) ++ (quickSort more)
    where
        less = filter (< x) xs
        equal = filter (== x) xs
        more = filter (> x) xs

forLoopy :: a -> (a -> Bool) -> (a -> a) -> (a -> IO ()) -> IO()
forLoopy index condition funct action = 
    if condition index
    then do
        action index
        forLoopy (funct index) condition funct action
    else return ()

-- A deconstruction of how this for loop works
-- An example of the function is forLoop 0 (<3) (+1) (print)
-- The first avlue that is taken is the index value for the entire forLoopy
-- It is altered by the value of the functions
-- In the example, the index is set to a value of 0
-- The next component is the condition, by this I mean what condition does the index have to meet to evaluate as 'True'
-- In this example, the index would first be is 0 less than three.
-- Given that is true we then want to act on index in some way
-- In this example, that action is to print the index to the screen
-- We then want to recursively another instance of the program, making a change to the index by applying it a function
-- In this example, that change would be to +1 to the value of the index, and run the function again
-- When the condition index is no longer true, we terminate the function

sequenceIO :: [IO a] -> IO [a]
sequenceIO [] = return []
sequenceIO (x:xs) = do
    v <- x
    vs <- sequenceIO xs
    return (v:vs)

-- So this one here takes types of a, with an applied IO function, and enacts that function on them

mapIO :: (a -> IO b) -> [a] -> IO [b]
mapIO funct [] = return []
mapIO funct (x:xs) = do
    v <- funct x
    vs <- mapIO funct xs
    return (v:vs)

-- So this one is where you take an IO method. This shows is show in the function (a -> IO b)
-- That function means to take a type a and output an IO function of b
-- The next thing is that it takes a list of type a
-- Finally this outputs the IO function


