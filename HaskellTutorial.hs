import Data.List
import System.IO

-- If you wanted to get something from another file
-- module SampFunctions (the name of function, the name of function) where
-- To have access to it
-- import SampFunctions

-- Int (range {-2^63, 2^63})
maxInt = maxBound :: Int
minInt = minBound :: Int

-- Integer (UnBounded whole number)
-- Float 
-- Double (Precision up to 11 points)
-- Bool
-- Char '
-- Tuple (can store lists of different data types)

always5 :: Int
always5 = 5

sumOfNums = sum [1..1000]

addEx = 5 + 4
subEx = 5 - 4
mulEx = 5 * 4
divEx = 5 / 4
modEx = mod 5 4 -- mod for modulus (using the prefix operator as it is at the start of the function)
modEx2 = 5 `mod` 4 -- mod for modulus (using the 'infix' operator as it is within the function)

negNumEx = 5 + (-4) -- The alternative (5 + -4) is not possible in Haskell

num9 = 9 :: Int
sqrtOf9 = sqrt (fromIntegral num9)

-- Built in math functions
piVal = pi
ePow9 = exp 9
logOf9 = log 9
squared9 = 9 ** 2
truncateVal = truncate 9.999
roundVal = round 9.999
ceilingVal = ceiling 9.999
floorVal = floor 9.999
-- Also sin, cos, tan, asin, acos, atan, sinh, cosh, tanh, asinh, acosh, atanh

trueAndFalse = True && False
trueOrFalse = True || False
notTrue = not(True)

primeNumbers = [2,3,5,7,11]
morePrimes = primeNumbers ++ [13,17,19]

favNums = 2 : 7 : 21 : 66 : []

multList = [[3,5,7],[11,13,17]]
morePrimes2 = 23 : morePrimes

lenPrime = length morePrimes2
revPrime = reverse morePrimes2
isListempty = null morePrimes2
secondPrime = morePrimes2 !! 1
firstPrime = morePrimes2 !! 0
firstPrime2 = head morePrimes2
lastPrime = last morePrimes2
primeInit = init morePrimes2
first3Primes = take 3 morePrimes2
removedPrimes = drop 3 morePrimes2

is7InList = 7 `elem` morePrimes2
maxPrime = maximum morePrimes2
minPrime = minimum morePrimes2
zeroToTen = [0..10]
evenList = [2,4..10]
letterList = ['A','C'..'Z']
infinPow10 = [10,20..]
many2s = take 10 (repeat 2)
many3s = replicate 10 3
cycleList = take 10 (cycle [1,2,3,4,5])

listTimes2 = [x * 2 | x <- [1..10]]
listTimes3 = [x * 3 | x <- [1..10], x * 3 <= 20]
divideBy9N13 = [x | x <- [1..500], x `mod` 13 == 0, x `mod` 9 == 0]
sortedList = sort [1,3,4,5,2]
sumOfLists = zipWith (+) [1,2,3,4,5] [6,7,8,9,10]
listBiggerThen5 = filter (>5) morePrimes
evensUpTo20 = takeWhile (<= 20) [2,4..]
multOfList = foldl (*) 1 [2,3,4,5] -- Could also be foldr which means to go right to left

pow3List = [ 3 ^ n | n <- [1..10]]
multTable = [[x * y | x <- [1..12]] | y <- [1..12]]
randTuple = (1,"random tuple")
bobSmith = ("Bob Smith",52)
bobsName = fst bobSmith
bobsAge = snd bobSmith
names = ["Bob","Mary","Steven"]
addresses = ["1 Real Street","2 Real Street","3 Real Street"]
namesNAddresses = zip names addresses

main = do
    putStrLn "What's your name"
    name <- getLine -- Takes information from the console
    putStrLn ("Hello " ++ name)

addMe :: Int -> Int -> Int
--funcName param1 param2 = operations (returned value)
addMe x y = x + y
sumMe x y = x + y
addTuples :: (Int, Int) -> (Int, Int) -> (Int, Int)
addTuples (x, y) (x2, y2) = (x + x2, y + y2)
whatAge :: Int -> String
whatAge 16 = "You can drive"
whatAge 18 = "You can vote"
whatAge 21 = "You're an adult"
whatAge _ = "Nothing important I guess"

factorial :: Int -> Int
factorial 0 = 1;
factorial n = n * factorial (n - 1)

isOdd :: Int -> Bool
isOdd n
    | n `mod` 2 == 0 = False
    | n `mod` 2 == 1 = True

isEven n = n `mod` 2 == 0
whatGrade :: Int -> String
whatGrade age 
    | (age >= 5) && (age <= 6) = "Kindergarten"
    | (age > 6) && (age <= 10) = "Primary School"
    | (age > 10) && (age <= 14) = "Intermediate School"
    | (age > 14) && (age <= 18) = "High School"
    | otherwise = "Go to college"

batAvgRating :: Double -> Double -> String
batAvgRating hits atBats
    | avg <= 0.200 = "Terrible batting average"
    | avg <= 0.250 = "Average Player"
    | avg <= 0.280 = "You're doing pretty good"
    | otherwise = "You're a superstar"
    where avg = hits / atBats

getListItems :: [Int] -> String
getListItems [] = "Your list is empty"
getListItems (x:[]) = "your list starts with " ++ show [x]
getListItems (x:y:[]) = "Your list contains " ++ show [x] ++ " and " ++ show [y]
getListItems (x:xs) = "The 1st item is " ++ show [x] ++ " and the rest are " ++ show xs

getFirstItem :: String -> String
getFirstItem [] = "Empty String"
getFirstItem all@(x:xs) = "The first letter in " ++ all ++ " is " ++ [x]

times4 :: Int -> Int
times4 x = x * 4
listTimes4 = map times4 [1,2,3,4,5]
multBy4 :: [Int] -> [Int]
multBy4 [] = []
multBy4 (x:xs) = times4 x : multBy4 xs

areStringsEq :: String -> String -> Bool -- [Char] is the same thing as a string
areStringsEq [] [] = True
areStringsEq (x:xs) (y:ys) = x == y && areStringsEq xs ys
areStringsEq _ _ = False

doMult :: (Int -> Int) -> Int
doMult func = func 3
num3Times4 = doMult times4

getAddFunc :: Int -> (Int -> Int)
getAddFunc x y = x + y
adds3 = getAddFunc 3
fourPlus3 = adds3 4

threePlusList = map adds3 [1..10]

dbl1To10 = map (\x -> x * 2) [1..10] -- A lambda equation

doubleEvenNumbers y = 
    if (y `mod` 2 /= 0)
        then y
        else y * 2

getClass :: Int -> String
getClass n = case n of 
    5 -> "Go to Kindy"
    6 -> "Go to Primary"
    _ -> "Go away"

-- enumerated type
data BaseballPlayer = Pitcher 
                    | Catcher 
                    | Infielder 
                    | Outfield
                    deriving Show 

barryBonds :: BaseballPlayer -> Bool
barryBonds Outfield = True

barryInOutField = print(barryBonds Outfield)

data Customer = Customer String String Double
    deriving Show

tomSmith :: Customer
tomSmith = Customer "Tom Smith" "1 Real Street" 20.50

getBalance :: Customer -> Double
getBalance (Customer _ _ b) = b

data RPS = Rock
         | Paper
         | Scissors

shoot :: RPS -> RPS -> String
shoot Paper Rock = "Paper beats Rock"
shoot Paper Scissors = "Paper loses to Scissors"
shoot Paper Paper = "Paper ties with Paper"
shoot Rock Rock = "Rock ties with Rock"
shoot Rock Scissors = "Rock beats Scissors"
shoot Rock Paper = "Rock loses to Paper"
shoot Scissors Rock = "Scissors loses to Rock"
shoot Scissors Scissors = "Scissors ties with Scissors"
shoot Scissors Paper = "Scissors beats Paper"
shoot _ _ = "Error"

data Shape = Circle Float Float Float
           | Rectangle Float Float Float Float
             deriving Show

area :: Shape -> Float
area (Circle _ _ r) = pi * r ^ 2
area (Rectangle x y x2 y2) = (abs (x2 - x)) * (abs (y2 - y))

sumValue = putStrLn (show (1 + 2))
sumValue2 = putStrLn . show $ 1 + 2

-- Type Classes examples: Num 

data Employee = Employee { name :: String,
                           position :: String,
                           idNum :: Int
    				     } deriving (Eq, Show)

samSmith = Employee { name = "Sam smith", position = "Manager", idNum = 1}
kimSmith = Employee { name = "Kim smith", position = "2IC", idNum = 2}

isSamKim = samSmith == kimSmith

samSmithData = show samSmith

data Shirtsize = S
               | M
               | L
			   
instance Eq Shirtsize where
	S == S = True
	M == M = True
	L == L = True
	_ == _ = False
			   
instance Show Shirtsize where
	show S = "Small"
	show M = "Medium"
	show L = "Large"

smallAvail = S `elem` [S,M,L]
theSize = show S

class MyEq a where
	areEqual :: a -> a -> Bool

instance MyEq Shirtsize where
	areEqual S S = True
	areEqual M M = True
	areEqual L L = True
	areEqual _ _ = False
	
newSize = areEqual M M

sayHello = do
   putStrLn "What's your name"
   name <- getLine
   putStrLn ("Hello " ++ name)
   
writeToFile = do
   theFile <- openFile "test.txt" WriteMode
   hPutStrLn theFile ("Random line of Text")
   hClose theFile

readFromFile = do
   theFile2 <- openFile "test.txt" ReadMode
   contents <- hGetContents theFile2
   putStr contents
   hClose theFile2
   
fib = 1 : 1 : [a + b | (a, b) <- zip fib (tail fib)]

Double x = x + x