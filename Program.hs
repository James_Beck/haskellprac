hypotenuse a b = sqrt (a ^ 2 + b ^ 2)

identifyCamel humps = if humps == 1 then "dromedary" else "Bactrian"

add1 :: Int -> Int
add1 x = x + 1

f :: (Int -> Int) -> Int
f x = 3

g :: Int -> (Int -> Int)
g x = add1

h :: Int -> (Int -> Int)
h x y = x + y

i :: Int -> (Int -> (Int -> (Int -> Int)))
i v x y z = v + x + y + z

add1ToEach :: [Int] -> [Int]
add1ToEach [] = []
add1ToEach (x:xs) = add1 x : add1ToEach xs

argmax :: (Ord b) => (a -> b) -> [a] -> a
argmax f [x] = x
argmax f (x:xs) = if f x > f (argmax f xs)
                  then x
                  else (argmax f xs)